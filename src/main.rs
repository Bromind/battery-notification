extern crate battery;
extern crate notify_rust;
extern crate clap;

use std::{thread, time};
use battery::{Manager, units, State};
use battery::units::Ratio;

use notify_rust::{Notification, NotificationUrgency};

use clap::{Arg, App};

fn battery_level() -> Ratio {
    Manager::new().unwrap()
        .batteries().unwrap()
        .next().unwrap().unwrap()
        .state_of_charge()
}

fn battery_state() -> State {
    Manager::new().unwrap()
        .batteries().unwrap()
        .next().unwrap().unwrap()
        .state()
}

#[derive(Eq, PartialEq, Ord, PartialOrd)]
enum BatteryThreshold {
    ABOVE,
    BELOW,
}

#[derive(Eq, PartialEq, Ord, PartialOrd)]
enum BatteryLevelVariation {
    INCREASE,
    DECREASE,
}

impl From<State> for BatteryLevelVariation {
    fn from(s: State) -> Self {
        match s {
            State::Discharging => BatteryLevelVariation::DECREASE,
            _ => BatteryLevelVariation::INCREASE,
        }
    }
}

struct BatteryState {
    threshold: BatteryThreshold,
    state: BatteryLevelVariation,
}

impl BatteryState {
    fn current_state(threshold_value: f32) -> Self {
        BatteryState {
            threshold: if battery_level().get::<units::ratio::percent>() > threshold_value {
                BatteryThreshold::ABOVE
            } else {
                BatteryThreshold::BELOW
            },
            state: BatteryLevelVariation::from(battery_state())
        }
    }
}

fn alert_low() {
    Notification::new()
        .appname("Battery alert")
        .summary("Low battery.")
        .body(&format!("Battery level: {}", battery_level().get::<units::ratio::percent>()))
        .urgency(NotificationUrgency::Critical)
        .finalize()
        .show()
        .unwrap();
}

fn alert_discharge() {
    Notification::new()
        .appname("Battery alert")
        .summary("Battery is now discharging and below threshold.")
        .body(&format!("Battery level: {}", battery_level().get::<units::ratio::percent>()))
        .urgency(NotificationUrgency::Normal)
        .finalize()
        .show()
        .unwrap();
}

struct AlertAutomata {
    threshold_value: f32,
    batt_state: BatteryState,
}

impl AlertAutomata {
    fn take_step(&mut self) {
        let new_batt_state = BatteryState::current_state(self.threshold_value);
        if self.batt_state.threshold == BatteryThreshold::ABOVE && new_batt_state.threshold == BatteryThreshold::BELOW
        {
                alert_low()
        } else if new_batt_state.threshold == BatteryThreshold::BELOW 
                    && self.batt_state.state == BatteryLevelVariation::INCREASE 
                    && new_batt_state.state == BatteryLevelVariation::DECREASE
        {
            alert_discharge()
        }
        
        self.batt_state = new_batt_state;
    }

    fn new(threshold_value: f32) -> Self {
        AlertAutomata {
            threshold_value,
            batt_state: BatteryState {
                threshold: BatteryThreshold::ABOVE,
                state: BatteryLevelVariation::INCREASE,
            }
        }
    }
}

fn main() {
    let matches = App::new("Battery Notification")
                          .version("0.0.2")
                          .author("Martin Vassor <martin.vassor@alumni.epfl.ch>")
                          .about("A low battery notification")
                          .arg(Arg::with_name("threshold")
                               .short("t")
                               .long("threshold")
                               .value_name("VALUE")
                               .help("Sets the notification threshold.")
                               .takes_value(true))
                          .get_matches();

    let threshold = matches.value_of("threshold")
        .unwrap_or("25.0")
        .parse::<f32>()
        .unwrap_or(25.0);

    let mut alert = AlertAutomata::new(threshold);
    loop {
        alert.take_step();
        let delay = time::Duration::new(1, 0);

        thread::sleep(delay);
    }
}
