# Battery-Notification

```
$ cargo run
```

You should receive a notification when your battery goes below 25%.

# Changelog

0.0.3: modifies the alert conditions. Now, there are two triggers: (i) while
	switching from above to below the threshold; or (ii) when switching from
	charging to discharging, while being under the threshold.
0.0.2: add the `-t` parameter to specify the alert threshold
